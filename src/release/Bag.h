/**
 * CSC232 - Data Structures with C++
 * Missouri State University, Spring 2017
 *
 * @file   Bag.h
 * @authors Frank M. Carrano
 *          Timothy M. Henry
 *			Alexander J. Byrd <zz2112@live.missouristate.edu>
 * @brief  Bag interface specification.
 *
 * @copyright Jim Daehn, 2017. All rights reserved.
 */

#ifndef HW01_BAG_H
#define HW01_BAG_H

#include <vector>

/**
 * @brief Encapsulates ADTs developed in CSC232, Data Structures with C++.
 */
namespace csc232 {

    /**
     * @brief Bag Interface introduced in Chapter 1 in Listing 1-1 with
     * modifications by Jim Daehn.
     * @tparam T class template parameter; the type of element stored in this
     * `Bag`.
     */
    template <typename T>
    class Bag {
    public:
        /**
         * @brief Gets the current number of entries in this bag.
         * @return The integer number of entries currently in this `Bag` is
         * returned.
         */
        virtual int getCurrentSize() const = 0;

        /**
         * @brief Determine whether this `Bag` is empty.
         * @return  True if this `Bag` is empty; false otherwise.
         */
        virtual bool isEmpty() const = 0;

        /**
         * @brief Adds a new entry to this `Bag`.
         * @post If successful, `newEntry` is stored in the bag and the count
         * of items in the bag has increased by 1.
         * @param newEntry The object to be added as a new entry.
         * @return True if addition was successful, or false if not
         */
        virtual bool add(const T& newEntry) = 0;

        /**
         * @brief Removes one occurrence of a given entry from this `Bag` if
         * possible.
         * @post If successful, `anEntry` has been removed from the bag and
         * the count of items i the bag has decreased by 1.
         * @param anEntry The entry to be removed.
         * @return True if removal was successful, or false if not.
         */
        virtual bool remove(const T& anEntry) = 0;

        /**
         * @brief Removes all entries from this bag.
         * @post Bag contains no items, and the count of items is 0.
         */
        virtual void clear() = 0;

        /**
         * @brief Counts the number of times a given entry appears in this bag.
         * @param anEntry The entry to be counted.
         * @return The number of times `anEntry` appears in the bag.
         */
        virtual int getFrequencyOf(const T& anEntry) const = 0;

        /**
         * @brief Tests whether this bag contains a given entry.
         * @param anEntry The entry to locate.
         * @return True if bag contains `anEntry`, or false otherwise.
         */
        virtual bool contains(const T& anEntry) const = 0;

        /**
         * @brief Empties and then fills a given `std::vector` with all
         * entries that are in this `Bag`.
         * @return A `std::vector` containing copies of all the entries in
         * this `Bag`.
         */
        virtual std::vector<T> toVector() const = 0;

	   /**
	    * @title unionOp
		* @brief Takes a bag object reference sent in, and the current bag object
		* that makes the method call, and uses the toVector() method to make a
		* vector of the entries in each bag, so that it can be iterated through and
		* all the entries of each bag can be unionized into one bag to be returned.
		* It uses the add method to construct the new unionized bag
		* @parameters bag2(Bag object reference)
		* @pre A Bag object reference is sent in to be unionized with the bag
		* object making the method call
		* @post Creates a new bag consisting of the entries in each bag, and 
		* returns it
		* @return A Bag with a union of both bags entries
		*/
		virtual Bag<T> unionOp(const Bag<T>& bag2) {

			std::vector<T> vec1 = this.toVector();
			std::vector<T> vec2 = bag2.toVector();

			//New bag which is empty
			Bag<T> temp = new Bag<T>();

			//Adds all entries from vec1 into the temp bag
			for (int i = 0; i < vec1.size(); i++) {
				temp.add(vec1.at(i));
			}

			//Adds all entries from vec2 into the temp bag
			for (int i = 0; i < vec2.size(); i++) {
				temp.add(vec2.at(i));
			}

			return temp;
		}

		/**
		* @title intersection
		* @brief Takes a bag object reference sent in, and the current bag object
		* that makes the method call, and uses the toVector() to convert the current
		* bag object (this) into a vector, and then iterates through that vector and
		* uses the contains() method to figure out which entries are contained in
		* both bags. Each time contains returns true for both, it is added into a
		* 3rd bag using the add() method to be returned as the intersection of
		* entries of the two bags. Also whenever a match is found, that particular
		* entry in bag2 is removed so that there are no duplicate intersections using
		* the remove() method.
		* @parameters bag2(Bag object reference)
		* @pre A Bag object reference is sent in containing entries
		* @post Creates a new bag consisting of entries that intersect (are the same)
		* in both bags
		* @return A Bag with the intersection of both bags entries
		*/
		virtual Bag<T> intersection(const Bag<T>& bag2) {

			std::vector<T> vector = this.toVector();

			//New bag that is empty
			Bag<T> temp = new Bag<T>();

			//Interates through all entries seeing if there is intersections
			for (int i = 0; i < vector.size(); i++) {

				//If both bags contain this entry
				if (bag2.contains(vector.at(i))) {

					//Add intersection to temporary bag
					temp.add(vector.at(i));

					//Remove one occurance of this entry in bag2
					bag2.remove(vector.at(i));
				}
			}

			return temp;
		}

		/**
		* @title difference
		* @brief Takes a bag object reference sent in, and the current bag object
		* that makes the method call, and uses the toVector() to convert the current
		* bag object (this) into a vector, and then iterates through that vector and
		* uses the contains() method to figure out which entries are contained in
		* both bags. Each time contains returns false, the means that it is a different
		* entry than what is in bag2, and it is added using the add() method into the
		* new bag to be returned. If contains() sends back true, remove that object from
		* bag2 so that for example if the current bag (this) has a,b,b,c and bag2 only
		* has b,d,e, then the second b in (this) bag would be a difference and not be
		* considered an intersection as well.
		* @parameters bag2(Bag object reference)
		* @pre A Bag object reference is sent in containing entries
		* @post Creates a new bag consisting of entries that are different
		* in the bag making the method call
		* @return A Bag with the difference between both bags entries
		*/
		virtual Bag<T> difference(const Bag<T>& bag2) {
			
			std::vector<T> vector = this.toVector();

			//New bag that is empty
			Bag<T> temp = new Bag<T>();

			//Interates through all entries seeing if there is intersections
			for (int i = 0; i < vector.size(); i++) {

				//If both bags contain this entry
				if (bag2.contains(vector.at(i))) {

					//Remove one occurance of this entry in bag2
					bag2.remove(vector.at(i));
				}
				else {

					//Add to temporary bag if it is a difference
					temp.add(vector.at(i));
				}
			}

			return temp;
		}

        /**
         * @brief Destroys this bag and frees its assigned memory.
         */
        virtual ~Bag() {
            // inlined, no-op
        }
    }; // end Bag
} // end namespace csc232

#endif //HW01_BAG_H
