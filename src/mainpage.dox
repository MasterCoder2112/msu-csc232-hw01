/**
@mainpage HW #1 Documentation

This documentation outlines the specifications for all publicly accessible
elements of the classes used in the `debug` and `release` sources. If you
cannot find the answer to some requirements specification here, consult your
HW #1 handout. If you still cannot find the answer to your question, either
raise it in a HipChat room, lecture or in an e-mail to your instructor.

<b>Exercise 6</b>

The \a union of two bags is a new bag containing the combined contents of the
original two bags. Design and specify a method \c union for the ADT bag that
returns as a new bag the union of the bag receiving teh call to the method
and the bag that is the method's one argument. Include sufficient comments
to fully specify the method.

Note that the union of two bags might contain duplicate items. For example,
if object \c x occurs five times in one bag and twice in another, the union
of these bags contains \c x seven times. Specifically, suppose that \c bag1
and \c bag2 are bags; \c bag1 contains the strings \c a, \c b, and \c c; and
\c bag2 contains the strings \c b, \c b, \c d, and \c e. The expression \c
bag1.union(bag2) returns a bag containing the strings \c a, \c b,  \c b, \c
b, \c c, \c d, \c e. Note that \c union does not affect the contents of \c
bag1 and \c bag2.

<b>Exercise 7</b>

The \a intersection of two bags is a new bag containing the entries that
occur in both of the original two bags. Design and specify a method \c
intersection for the ADT bag that returns as a new bag the intersection of
the bag receiving the call to the method and the bag that is the method's one
argument. Include sufficient comments to fully specify the method.

Note that the intersection of two bags might contain duplicate items. For
example, if object \c x occurs five times in one bag and twice in another,
the intersection of these bags contains \c x two times. Specifically, suppose
that \c bag1 and \c bag2 are bags; \c bag1 contains the strings \c a, \c b,
and \c c; and \c bag2 contains the strings \c b, \c b, \c d, and \c e. The
expression \c bag1.intersection(bag2) returns a bag containing only the
string \c b. Note that \c intersection does not affect the contents of \c
bag1 and \c bag2.

<b>Exercise 8</b>

The \a difference of two bags is a new bag containing the entries that
would be left in one bag after removing those that also occur in the second.
Design and specify a method \c difference for the ADT bag that returns as a
new bag the difference of the bag receiving the call to the method and the
bag that is the method's one argument. Include sufficient comments to fully
specify the method.

Note that the difference of two bags might contain duplicate items. For
example, if object \c x occurs five times in one bag and twice in another,
the difference of these bags contains \c x three times. Specifically, suppose
that \c bag1 and \c bag2 are bags; \c bag1 contains the strings \c a, \c b,
and \c c; and \c bag2 contains the strings \c b, \c b, \c d, and \c e. The
expression \c bag1.difference(bag2) returns a bag containing only the
strings \c a and \c c. Note that \c difference does not affect the contents
of \c bag1 and \c bag2.

<b>Programming Problem 6</b>

Exercises 6, 7, and 8 ask you to specify methods for the ADT bag that return
the union, intersection and difference of two bags. Define each of these
methods independently of the bag's implementation by using only ADT bag
operations.

<b>Submission Details</b>

As usual, create a pull request for your implementation and provide a link to
the pull request in Blackboard.

<b>Due Date</b>: 23:59:59 02 February 2017.

*/

