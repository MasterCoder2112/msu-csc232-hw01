# Homework Assignment #1

## Chapter 1 Data Abstraction - Additional Bag Operations

## Due: 23:59 Thursday 02 February 2017

## Points: 5

## To Do…

1. Fork https://bitbucket.org/professordaehn/msu-csc232-hw01 into a private repo in your Bitbucket account as per our standard operating procedure.
1. Checkout your forked repo and checkout the develop branch to do your work.
1. Locate and implement the task outlined in the `TODO` comments for Exercise 6
 in `Bag.h` and commit your work as specified when done.
1. Locate and implement the task outlined in the `TODO` comments for Exercise 7
  in `Bag.h` and commit your work as specified when done.
1. Locate and implement the task outlined in the `TODO` comments for Exercise 8
 in `Bag.h` and commit your work as specified when done.
1. Locate and implement the task outlined in the `TODO` comments for 
Programming Problem 6 in `Bag.h` and commit your work as specified when done.
1. Create a pull request to merge your develop branch into the master branch on your private repo. Any pull requests made on my original repo will be declined and you will receive no credit for this assignment.
Log onto Blackboard and navigate to Content > Homework to find the link to submit this assignment for grading. Submit the assignment by entering the URL of your pull request -- as a working hyperlink -- in the Text Submission field of the assignment. It is the timestamp on this last step that dictates whether the assignment as been submitted on time or not. That is, regardless of when the commits were made to your repo, the assignment is considered late if not submitted on Blackboard by the due date

If you have *any* questions, do not hesitate to call, text, video chat, etc. me.

## Grading

1. (1 point) Pull Request: Did the student
    1. properly fork my repo into a private repo in their account?
    1. create a develop branch within which to do the work?
    1. create a pull request on their private repo?
    1. submit assignment to Blackboard on time?
1. (1 point) Comment Quality: Did the student
    1. use proper grammar -- including spelling -- in their comments?
    1. write comments that fully specify each operation?
    1. use the appropriate Doxygen tags (e.g., `@param`, `@pre`, `@post`, etc. See Appendix I) to support the documentation?
1. (3 points) Exercises and Programming Problem
    1. (0.5 points) Exercise 6
    1. (0.5 points) Exercise 7
    1. (0.5 points) Exercise 8
    1. (1.5 points) Programming Problem 6
